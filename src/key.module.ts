import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from './config/config.module';
import { KeyService } from './key.service';
import { KeyController } from './key.controller';
import { AppConfig } from './config/config.service';
import { EncryptedKeyStore } from './entities/key.entity';

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [],
      inject: [AppConfig],
      useFactory: async (config: AppConfig) => {
        return {
          type: 'postgres',
          url: config.dbURL,
          synchronize: false,
          autoLoadEntities: true,
        };
      },
    }),
    TypeOrmModule.forFeature([EncryptedKeyStore]),
  ],
  controllers: [KeyController],
  providers: [KeyService],
  exports: [],
})
export class KeyModule {}
