import { NestFactory } from '@nestjs/core';
import { KeyModule } from './key.module';

async function bootstrap() {
  const app = await NestFactory.create(KeyModule);
  await app.listen(3000);
}
bootstrap();
