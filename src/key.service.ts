import {
  Injectable,
  OnApplicationBootstrap,
  OnModuleInit,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { JWK } from 'node-jose';
import { v4 as uuid } from 'uuid';
import { KeyStoreAlreadyExistsError } from './errors/keyStoreAlreadyExists.error';
import { EncryptedKeyStore } from './entities/key.entity';
import { Repository } from 'typeorm';
import { AppConfig } from './config/config.service';
import { createDecipheriv, createCipheriv, scrypt, randomBytes } from 'crypto';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class KeyService implements OnModuleInit {
  private readonly logger = new Logger(KeyService.name, true);
  private readonly repo: Repository<EncryptedKeyStore>;
  private readonly config: AppConfig;
  keyStore: Record<string, JWK.KeyStore> = {};

  constructor(
    @InjectRepository(EncryptedKeyStore) repo: Repository<EncryptedKeyStore>,
    config: AppConfig,
  ) {
    this.repo = repo;
    this.config = config;
  }

  async onModuleInit() {
    await this.loadKeyStores();
  }

  /**
   * Return a keystore
   * @param id The id of the keystore
   */
  getKeyStore(id: string): JWK.KeyStore {
    return this.keyStore[id] || null;
  }

  /**
   *
   */
  async createKeyStore(id?: string) {
    // Fallback to uuid
    if (typeof id !== 'string') {
      id = uuid();
    }

    // Check for duplicate
    if (this.getKeyStore(id) !== null) {
      throw new KeyStoreAlreadyExistsError();
    }

    const keystore = JWK.createKeyStore();
    this.keyStore[id] = keystore;

    // Commit keystore to database
    await this.CommitKeyStore(id);

    return id;
  }

  /**
   * Delete a keystore
   * @param id the ID of the keystore to remove
   */
  async deleteKeystore(id: string) {
    // Check if keystore exists
    if (this.keyStore[id] === null || this.keyStore[id] === undefined) {
      throw new NotFoundException();
    }

    // Delete from memory and database
    delete this.keyStore[id];
    await this.repo
      .createQueryBuilder()
      .delete()
      .where('id = :id', { id })
      .execute();
  }

  /**
   * Generate a new RSA key
   */
  async generateKey(keyStoreId: string): Promise<string> {
    const store = this.getKeyStore(keyStoreId);
    if (store === null) {
      throw new NotFoundException();
    }

    const key = await store.generate('RSA', 512);

    // Commit to database
    await this.CommitKeyStore(keyStoreId);

    return key.kid;
  }

  /**
   * Returns a key from a keystore in the chosen format
   * @param keyStoreId
   * @param keyId
   * @param format
   */
  async getKey(
    keyStoreId: string,
    keyId: string,
    privateKey: boolean = false,
    format: 'JWK' | 'PEM' = 'JWK',
  ): Promise<string | object> {
    const keystore = this.getKeyStore(keyStoreId);
    if (keystore === null) return null;

    const rawKey = keystore.get(keyId);
    if (rawKey === null) return null;

    const key = await JWK.asKey(rawKey);
    if (format === 'JWK') {
      return key.toJSON(privateKey);
    } else {
      return key.toPEM(privateKey);
    }
  }

  /**
   * Encrypts the keystore with given ID from memory and inserts or updates it into the database
   * @param id The keystore ID
   */
  protected async CommitKeyStore(id: string) {
    const keystore = this.getKeyStore(id);

    // Stringify JWK (will become JWKS)
    const keystoreJSON = JSON.stringify(keystore.toJSON(true));

    // Encrypt keystore into database
    const key = await this._scrypt(this.config.aesKey, '', 32);
    const iv = randomBytes(32);
    const cipher = createCipheriv('aes-256-gcm', key, iv);
    let encrypted = cipher.update(keystoreJSON, 'utf8', 'base64');
    encrypted += cipher.final('base64');
    const authTag = cipher.getAuthTag();

    // Save into database
    const count = await this.repo
      .createQueryBuilder()
      .where('id = :id', { id })
      .getCount();

    // Update or insert
    if (count > 0) {
      await this.repo
        .createQueryBuilder()
        .where('id = :id', { id })
        .update({
          authTag: authTag.toString('base64'),
          iv: iv.toString('base64'),
          keystore: encrypted,
        })
        .execute();
    } else {
      await this.repo
        .createQueryBuilder()
        .insert()
        .values({
          id,
          authTag: authTag.toString('base64'),
          iv: iv.toString('base64'),
          keystore: encrypted,
        })
        .execute();
    }
  }

  /**
   * Loads an encrypted keystore into memory
   * @param store The encrypted keystore
   */
  protected async loadKeyStore(store: EncryptedKeyStore) {
    const key = await this._scrypt(this.config.aesKey, '', 32);
    const encrypted = store.keystore;
    const authTag = Buffer.from(store.authTag, 'base64');
    const iv = Buffer.from(store.iv, 'base64');

    // Decrypt JWKS
    const decipher = createDecipheriv('aes-256-gcm', key, iv);
    decipher.setAuthTag(authTag);
    let decrypted = decipher.update(encrypted, 'base64', 'utf8');
    decrypted += decipher.final('utf8');

    // Build keystore
    const jwks = JSON.parse(decrypted);
    const keystore = await JWK.asKeyStore(jwks);

    // Store in memory
    this.keyStore[store.id] = keystore;
  }

  /**
   * Load keystores in database into local memory
   */
  protected async loadKeyStores() {
    const keystores = await this.repo.createQueryBuilder().getMany();
    this.logger.log(
      `KeyService is loading ${keystores.length} encrypted keystores from the database.`,
    );

    try {
      await Promise.all(keystores.map(this.loadKeyStore, this));
    } catch (e) {
      this.logger.error(
        'Failed to load or decrypt keystore from database, this can be caused by having an incorrect AES_KEY set in the config.',
        e,
      );
    }
  }

  /**
   * Wraps the scrypt function to use async/await
   * @param password
   * @param salt
   * @param keyLen
   */
  private async _scrypt(
    password: string,
    salt: string,
    keyLen: number,
  ): Promise<Buffer> {
    return new Promise((resolve, reject) => {
      scrypt(password, salt, keyLen, (err, derivedKey) => {
        if (err) {
          reject(err);
        }
        resolve(derivedKey);
      });
    });
  }
}
