import {
  Controller,
  Get,
  Param,
  Post,
  Query,
  NotFoundException,
  BadRequestException,
  Body,
  Delete,
} from '@nestjs/common';
import { KeyService } from './key.service';
import { CreateKeyStoreDTO } from './dto/createKeyStore.dto';
import { KeyStoreAlreadyExistsError } from './errors/keyStoreAlreadyExists.error';

@Controller()
export class KeyController {
  constructor(private readonly service: KeyService) {}

  @Post('/')
  async createKeyStore(@Body() dto: CreateKeyStoreDTO) {
    try {
      const id = await this.service.createKeyStore(dto.id || null);
      await this.service.generateKey(id);

      return {
        id,
      };
    } catch (e) {
      if (e instanceof KeyStoreAlreadyExistsError) {
        throw new BadRequestException(e.message);
      }
      throw e;
    }
  }

  /**
   * Get a keystore with public keys
   * @param id the keystore its id
   */
  @Get('/public/:id')
  getPublicKeystore(@Param('id') id: string) {
    const store = this.service.getKeyStore(id);
    if (store === null) {
      throw new NotFoundException();
    }

    //
    return store.toJSON();
  }

  /**
   * Get a keystore with private keys
   * @param id the keystore its id
   */
  @Get('/private/:id')
  getPrivateKeystore(@Param('id') id: string) {
    const store = this.service.getKeyStore(id);
    if (store === null) {
      throw new NotFoundException();
    }

    //
    return store.toJSON(true);
  }

  /**
   *
   */
  @Delete(['/public/:id', '/private/:id'])
  deleteKeystore(@Param('id') id: string) {
    return this.service.deleteKeystore(id);
  }

  /**
   * Get a public key from a keystore
   * @param keyStoreId
   * @param keyId
   * @param format
   */
  @Get('/public/:keyStoreId/:keyId')
  async getPublicKey(
    @Param('keyStoreId') keyStoreId: string,
    @Param('keyId') keyId: string,
    @Query('format') format = 'jwk',
  ) {
    return this.getKey(keyStoreId, keyId, false, format);
  }

  /**
   * Get a private key from a keystore
   * @param keyStoreId
   * @param keyId
   * @param format
   */
  @Get('/private/:keyStoreId/:keyId')
  async getPrivateKey(
    @Param('keyStoreId') keyStoreId: string,
    @Param('keyId') keyId: string,
    @Query('format') format = 'jwk',
  ) {
    return this.getKey(keyStoreId, keyId, true, format);
  }

  /**
   * Get a key from a keystore
   * @param keyStoreId
   * @param keyId
   * @param privateKey whether to return the private key, otherwise returns the public key
   * @param format the format to return the key in
   */
  private async getKey(
    keyStoreId: string,
    keyId: string,
    privateKey: boolean,
    format = 'jwk',
  ) {
    format = format.toUpperCase();
    if (format !== 'JWK' && format !== 'PEM') {
      throw new BadRequestException();
    }

    const keyData = await this.service.getKey(
      keyStoreId,
      keyId,
      privateKey,
      format,
    );
    if (keyData === null) {
      throw new NotFoundException();
    }

    return {
      data: keyData,
    };
  }
}
