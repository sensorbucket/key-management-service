export class KeyStoreAlreadyExistsError extends Error {
  constructor() {
    super('A KeyStore with given id already exists!');
  }
}
