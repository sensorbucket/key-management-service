import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppConfig {
  private logger = new Logger(AppConfig.name);

  constructor(private readonly config: ConfigService) {}

  //
  //  Database configuration
  //

  get dbURL() {
    let value = this.config.get('DB_URL', undefined);
    if (value === undefined) {
      this.logger.error('Database URL not configured!');
      process.exit(-1);
    }
    return value;
  }
  //
  // Key settings
  //

  get aesKey() {
    const value = this.config.get('AES_KEY', undefined);

    // Assert that value is set
    if (value === undefined) {
      this.logger.error('AES_KEY is not configured!');
      process.exit(-1);
    }

    return value;
  }
}
