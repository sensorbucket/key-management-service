import { Module, Global } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import { AppConfig } from './config.service';

@Global()
@Module({
  imports: [NestConfigModule.forRoot()],
  providers: [AppConfig],
  exports: [AppConfig],
})
export class ConfigModule {}
