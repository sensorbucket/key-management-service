FROM node:12 AS build

WORKDIR /opt/app

COPY package.json .
COPY yarn.lock .
RUN yarn install \
  && yarn cache clean

COPY . .

RUN yarn build \
  && yarn install --production

#
#
#
FROM node:12-slim AS PRODUCTION
ARG SERVICE
WORKDIR /opt/app

COPY --from=build /opt/app/ ./

CMD ["yarn", "start"]