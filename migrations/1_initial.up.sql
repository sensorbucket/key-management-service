CREATE TABLE encrypted_key_store (
  "id"        varchar    NOT NULL PRIMARY KEY,
  "keystore"  varchar    NOT NULL,
  "authTag"   varchar    NOT NULL,
  "iv"        varchar    NOT NULL
);